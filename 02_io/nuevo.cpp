#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){
    int edad;
    char nombre[32]; 
    printf("Dime tu nombre: "); 
    scanf(" %s", nombre);
    printf("Dime tu edad: ");
    scanf(" %i", &edad);
    printf("\n");
    printf("Tu nombre es %s\n", nombre);
    printf("Tu edad es %i\n", edad);
    return EXIT_SUCCESS;
}
