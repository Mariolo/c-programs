#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1000
#define BTU   10000 /* Basic Time Unit */
 
/* Función punto de entrada */
int  main(){
	int duracion[1000] = {0, 2, 8, 2, 1, 8, 2, 1, 2, 1, 2, 2}; 

	// bucle for: Debe valer repetir cosas
	for (int i=0; i<VECES; i++) {
	    usleep(duracion [i] * BTU);
	    fputc('\a', stderr);
	    
        }

	return EXIT_SUCCESS;
}

