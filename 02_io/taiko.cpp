#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Función punto de entrada */
int  main(){

	/* printf tiene escritura bufferizada */
	// puts("hola");
	// printf("hola\n");
	// printf("Hola, %s. Que tal estas? %X\n", "Pepe", (int) '0');
	fprintf(stderr, "\a"); /* stderr no está bufferizado */
	usleep(100000);
	fputc('\a', stderr);
	usleep(100000);
	printf("\a\n");

    return EXIT_SUCCESS;
}

